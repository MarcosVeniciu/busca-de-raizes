import math  # funcoes matematicas https://docs.python.org/3/library/math.html


casas_decimais = 7

def funcao(x):
    reultado = math.pow(x,2) + math.log(x)

    return round(reultado, casas_decimais) # O 7 e numero de casas apos a virgula


def Bissecao():
    A = 1/5 # Valor de a para o intevalo inicial
    B = 1 # Valor de b para o intevalo inicial
    precisao = 0.001 # Valor da precisao E

    equacao = "x^2 + ln(x)"


    iteracao = 0 #representa o numero da iteracao, primeira, segunda, terceira ....
    iteracao_maxima = 20

    cabecalho(equacao, A, B, precisao)

    # Para o i_0

    if(B-A)< precisao:
        Condicao_parada = "Sim"
        resultado_final = round((A + B)/2 , 7) # O 7 e numero de casas apos a virgula

        Continuar = False # Parar não entrar no while
        iteracoes(iteracao, A, B, "-", "-", "-", Condicao_parada, resultado_final)
    else:
        Continuar = True
        iteracoes(iteracao, A, B, "-", "-", "-", "Não", "-")
        iteracao = iteracao + 1

    # Para o i de 1 ate n
    while Continuar:

        X = round((A + B)/2 , casas_decimais) # numero de casas apos a virgua
        F_a = funcao(A)
        F_x = funcao(X)

        if F_a * F_x > 0: # verifica se o a ou o b sera substituido pelo X
            A = X
        else:
            B = X


        if(B-A)< precisao:
            Condicao_parada = "Sim"
            resultado_final = round((A + B)/2 , casas_decimais) # numero de casas apos a virgula

            Continuar = False # Interrompe o loop
        else:
            Condicao_parada = "Não"
            resultado_final = "-"



        iteracao = iteracao + 1
        if iteracao == iteracao_maxima: #Limita o numero de execuçoes caso não ache uma raiz
            Continuar = False
            resultado_final = round((A + B)/2 , casas_decimais)
            iteracoes(iteracao, A, B, X, F_a, F_x, Condicao_parada, resultado_final)
        else:
            iteracoes(iteracao, A, B, X, F_a, F_x, Condicao_parada, resultado_final)
    print("")
    print("")



def cabecalho(equacao, A, B, precisao):
    print("")
    print("  Metódo da Bissecção")
    print("")
    print("")

    print(f'{"  A: ":<3} {A:<10} {"    ":^4} {"B: ":<3} {B:<10} {"    ":^4} {"Epsilon: ":<3} {precisao:<10} ')
    print("")
    print("  Função: " + equacao)

    print("")
    print("")
    espaco = "      " # 6 espacos
    string_1 = "  " + "i" + espaco + "a      " + espaco + " b      " + espaco + " x      "
    string_2 = espaco + " F(a)      " + espaco + " F(x)      " + espaco + "(b-a) < epsilon" + espaco + "Resultado Final"
    print(string_1 + string_2)
    print("--------------------------------------------------------------------------------------------------------------------------")


def iteracoes(iteracao, A, B, X, f_a, f_x, Condicao_parada, resultado_final):
    print( f'{iteracao:^5} {" ":^2} {A:<10} {" ":^2} {B:<10} {" ":^2} {X:<10} {" ":^2} {f_a:<10}   {" ":^1}   {f_x:<10} {" ":^1}   {Condicao_parada:^17} {" ":^2} {resultado_final:^10} ' )
    print("--------------------------------------------------------------------------------------------------------------------------")

    # f'{iteracao:^5} ' = f'{}' estrutura pra formatacao dentro fica a " iteracao: " é a variavel seguido de : e o numero de espaços que a variavel vai ocupar
    # ^ significa que o que esta na variavel vai ficar centralizado nos 5 espaços
    # < significa que o que esta na variavel vai ficar alinhado a esquerda nos 5 espaços
    # > significa que o que esta na variavel vai ficar alinhado a direita  nos 5 espaços
