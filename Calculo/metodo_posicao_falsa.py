import math  # funcoes matematicas https://docs.python.org/3/library/math.html
casas_decimais = 7

def funcao(x):
    reultado = math.pow(x, 3) + (2*math.exp(x))

    return round(reultado, casas_decimais) # O numero de casas apos a virgula

def PosicaFalsa():
    A = -2 # Valor de a para o intevalo inicial
    B = 0 # Valor de b para o intevalo inicial
    precisao_1 = 0.0001 # Valor da precisao E_1
    precisao_2 = 0.0001 # Valor da precisao E_2

    equacao = "x^3 + 2*e^x"

    iteracao = 0 #representa o numero da iteracao, primeira, segunda, terceira ....
    iteracao_maxima = 5

    cabecalho(equacao, A, B, precisao_1, precisao_2)

    # Para o i_0

    if (B-A < precisao_1): # primeira  condiçao de parada (b-a) < epsilon_1
        Continuar = False
        resultado_final = round((A + B)/2 , casas_decimais)# valor entre a e b
        iteracoes(iteracao, A, B, "-", "-", "-", "-", "Sim", "Nao", "Nao", "Nao", resultado_final)

    else:
        if abs(funcao(A)) < precisao_2: # segunda condiçao de parada |f(a)| < epsilon_2
            Continuar = False
            resultado_final = A
            iteracoes(iteracao, A, B, "-", "-", "-", "-", "Não", "Sim", "Nao", "Nao", resultado_final)

        else:
            if abs(funcao(B)) < precisao_2: # terceira condiçao de parada |f(b)| < epsilon_2
                Continuar = False
                resultado_final = B
                iteracoes(iteracao, A, B, "-", "-", "-", "-", "Não", "Nao", "Sim", "Nao", resultado_final)

            else: # caso nenhuma das tres seja atendida
                Continuar = True
                iteracoes(iteracao, A, B, "-", "-", "-", "-", "Não", "Nao", "Nao", "Nao", "-")
                iteracao = iteracao + 1

    # Para o i de 1 ate n
    while Continuar: # termina quando Terminar for falso
        X = round( (( (A*funcao(B)) - (B* funcao(A))) / (funcao(B) - funcao(A)))   , casas_decimais) # numero de casas apos a virgula

        if abs(funcao(X)) < precisao_2: # quarta condicao de parada |f(x)| < epsilion 2
            resultado_final = X
            Continuar = False
            iteracoes(iteracao, A, B, X, funcao(A), funcao(B), funcao(X), "Não", "Nao", "Nao", "Sim", resultado_final)

        else:

            F_a = funcao(A)
            F_b = funcao(B)
            F_x = funcao(X)

            if F_a * F_x > 0: # verifica se o a ou o b sera substituido pelo X
                A = X
            else:
                B = X

            # Verifica as tres condicoes de parada
            if (B-A < precisao_1): # primeira  condiçao de parada (b-a) < epsilon_1
                Continuar = False
                resultado_final = round ((A + B)/2 , casas_decimais) # valor entre a e b

                iteracoes(iteracao, A, B, X, F_a, F_b, F_x, "Sim", "Nao", "Nao", "Nao", resultado_final)

            else:
                if abs(funcao(A)) < precisao_2: # segunda condiçao de parada |f(a)| < epsilon_2
                    Continuar = False
                    resultado_final = A
                    iteracoes(iteracao, A, B, X, F_a, F_b, F_x, "Não", "Sim", "Nao", "Nao", resultado_final)

                else:
                    if abs(funcao(B)) < precisao_2: # terceira condiçao de parada |f(b)| < epsilon_2
                        Continuar = False
                        resultado_final = B
                        iteracoes(iteracao, A, B, X, F_a, F_b, F_x, "Não", "Nao", "Sim", "Nao", resultado_final)

                    else:
                        Continuar = True
                        iteracoes(iteracao, A, B, X, F_a, F_b, F_x, "Não", "Nao", "Nao", "Nao", "-")
                        iteracao = iteracao + 1
                        if iteracao == iteracao_maxima: #Limita o numero de execuçoes caso não ache uma raiz
                            Continuar = False
                            resultado_final = round ((A + B)/2 , casas_decimais) # valor entre a e b
                            iteracoes(iteracao, A, B, X, F_a, F_b, F_x, "Não", "Nao", "Nao", "Nao", resultado_final)
    print("")
    print("")


def cabecalho(equacao, A, B, precisao_1, precisao_2):
    print("")
    print("  Metódo da Posição Falsa")
    print("")
    print("")

    print(f'{"  A: ":<3} {A:<10} {"    ":^4} {"B: ":<3} {B:<10} {"    ":^4} {"Epsilon_1: ":<3} {precisao_1:<10} {"    ":^4} {"Epsilon_2: ":<3} {precisao_2:<10} ')
    print("")
    print("  Função: " + equacao)

    print("")
    print("")
    espaco = "   " # 3 espacos
    string_1 = "  " + "i" + espaco + "a      " + espaco + " b      " + espaco + " x      "
    string_2 = espaco + " F(a)      " + espaco + " F(b)      " + espaco + " F(x)      " + espaco + "(b-a)< E_1"
    string_3 =  espaco + "|f(a)|< E_2" + espaco + "|f(b)|< E_2" + espaco + "|f(x)|< E_2" + espaco + "Resultado"
    print(string_1 + string_2 + string_3)
    print("-------------------------------------------------------------------------------------------------------------------------------------------------")


def iteracoes(iteracao, A, B, X, f_a, f_b, f_x, parada_1, parada_2, parada_3, parada_4, resultado_final):
    print( f'{iteracao:^5} {A:<10} {B:<10} {X:<10} {f_a:<10} {" ":^3}{f_b:<10} {" ":^3}{f_x:<10} {" ":^5}{parada_1:<10} {" ":^3}{parada_2:<10} {" ":^3}{parada_3:<10} {" ":^3}{parada_4:<10} {resultado_final:<10}  ' )
    print("-------------------------------------------------------------------------------------------------------------------------------------------------")

    # f'{iteracao:^5} ' = f'{}' estrutura pra formatacao dentro fica a " iteracao: " é a variavel
    # seguido de : e o numero de espaços que a variavel vai ocupar
    # ^ significa que o que esta na variavel vai ficar centralizado nos 5 espaços
    # < significa que o que esta na variavel vai ficar alinhado a esquerda nos 5 espaços
    # > significa que o que esta na variavel vai ficar alinhado a direita  nos 5 espaços
