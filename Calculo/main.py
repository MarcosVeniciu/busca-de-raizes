import metodo_bisseccao
import metodo_posisao_falsa
import metodo_secante
import metodo_newton_raphson
import metodo_ponto_fixo
import converter_numeros
import os


os.system('clear') or None #limpar a tela sempre que for executado

print("1 - Método da Bisseção")
print("2 - Método da Posição falsa")
print("3 - Método da Secante")
print("4 - Método de newton-Raphson")
print("5 - Método do Ponto fixo")
print("6 - Converter Binario em Decimal")
print("7 - Converter Decimal fracionario em Binario")
print()
print("Escolha o método: ")
metodo = int(input())



os.system('clear') or None #limpar a tela sempre que for executado

if metodo == 1: metodo_bisseccao.Bissecao()
if metodo == 2: metodo_posisao_falsa.PosicaFalsa()
if metodo == 3: metodo_secante.Secante()
if metodo == 4: metodo_newton_raphson.NewtonRaphson()
if metodo == 5: metodo_ponto_fixo.PontoFixo()
if metodo == 6: converter_numeros.binario_decimal()
if metodo == 7: converter_numeros.decimal_fracionario_binario()
