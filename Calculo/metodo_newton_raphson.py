import math  # funcoes matematicas https://docs.python.org/3/library/math.html
casas_decimais = 7

def funcao(x):
    reultado = math.pow(x, 3) + (2*math.exp(x))

    return round(reultado, casas_decimais) # O numero de casas apos a virgula

def funcao_derivada(x):
    derivada = 3*math.pow(x, 2) + (2*math.exp(x))

    return round(derivada, casas_decimais) # O numero de casas apos a virgula

def NewtonRaphson():
    A = -2
    B = 0
    x = -1.99

    precisao_1 = 0.0001
    precisao_2 = 0.0001


    equacao = "x^3 + 2*e^x"
    derivada_equacao = "3x^2 + 2*e^x"

    iteracao = 0
    iteracao_maxima = 10
    cabecalho(equacao, derivada_equacao, A, B, x, precisao_1, precisao_2)


    # Para X_0
    if abs(funcao(x)) < precisao_1: # Verifica a primeira condição de parada
        iteracoes(iteracao, x, funcao(x), funcao_derivada(x), "Sim", "-", x)
        Continuar = False
    else:
        Continuar = True
        iteracoes(iteracao, x, funcao(x), funcao_derivada(x), "Não", "-", "-")
        iteracao = iteracao +1

    # Para x_1 ate x_n
    while Continuar:
        x_anterior = x
        x = round((x_anterior - (funcao(x_anterior)/ funcao_derivada(x_anterior))), casas_decimais) # O numero de casas apos a virgula

        if abs(funcao(x))< precisao_1: # Verifica a primeira condição de parada
            iteracoes(iteracao, x, funcao(x), funcao_derivada(x), "Sim", "Não", x)
            Continuar = False
        else:
            if abs(x - x_anterior) < precisao_2: # Verifica a segunda condicao de parada
                iteracoes(iteracao, x, funcao(x), funcao_derivada(x), "Não", "Sim", x)
                Continuar = False
            else:
                iteracoes(iteracao, x, funcao(x), funcao_derivada(x), "Não", "Não", "-")
                iteracao = iteracao +1
                if iteracao == iteracao_maxima: #Limita o numero de execuçoes caso não ache uma raiz
                    Continuar = False
                    iteracoes(iteracao, x, funcao(x), funcao_derivada(x), "Não", "Não", x)

    print("")
    print("")





def cabecalho(equacao, derivada_equacao, A, B, X, precisao_1, precisao_2):
    print("")
    print("  Metódo de Newton-Raphson")
    print("")
    print("")
    print(" A: {0:<10}  B: {1:<10}  X: {2:<10}".format(A, B, X))
    print("")
    print(" Epsilon_1: {0:<10}  Epislon_2: {1:<10}".format(precisao_1, precisao_2))
    print("")
    print(" Função: {0}".format(equacao))
    print("")
    print(" Derivada da Função: {0}".format(derivada_equacao))
    print("")
    print("")
    print("  i   X            F(x)        F'(x)       |f(X)|< E_1    |X_i - X_(i-1)|< E_2     Resultado" )
    print("-------------------------------------------------------------------------------------------")



def iteracoes(iteracao, X, f_x, df_x, parada_1, parada_2, resultado):

    # tenta por float sempre vai ter um numero exeto para resultado fianl
    print("  {0:<2}  {1:<10.7f}   {2:<10.7f}  {3:<10.7f}  {4:^11}    {5:^20}     {6:^9}".format(iteracao, X, f_x, df_x, parada_1, parada_2, resultado))
    print("-------------------------------------------------------------------------------------------")
    #https://programadorviking.com.br/metodo-format-em-python/
