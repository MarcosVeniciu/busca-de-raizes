import math  # funcoes matematicas https://docs.python.org/3/library/math.html
casas_decimais = 7

def funcao(x):
    reultado = 2*math.pow(x, 3) + math.exp(x)

    return round(reultado, casas_decimais) # O numero de casas apos a virgula


def Secante():
    A = -2
    B = 0
    X_anterior = -1.99 # X_0
    X_atual = -0.01   # X_1

    precisao_1 = 0.0001
    precisao_2 = 0.0001


    equacao = "2*x^3 + e^x"

    iteracao = 0
    iteracao_maxima = 20
    cabecalho(equacao, A, B, X_anterior, X_atual, precisao_1, precisao_2)

    # Para X_0
    if abs(funcao(X_anterior)) < precisao_1: # primeira condicao de parada para X_anterior |f(X_i)| < E_1
        resultado_final = X_anterior

        Continuar = False # Parar não entrar no while
        iteracoes(iteracao, X_anterior, funcao(X_anterior), "Sim", "-", resultado_final)
    else: # X_0 não é raiz
        Continuar = True
        iteracoes(iteracao, X_anterior, funcao(X_anterior), "Não", "-", "-")
        iteracao = iteracao + 1


    # Para X_1
    if Continuar == True: # se X_0 for raiz ele não vai verificar para X_1
        if abs(funcao(X_atual)) < precisao_1: # primeira condicao de parada para X_atual |f(X_i)| < E_1
            resultado_final = X_atual

            Continuar = False # Parar não entrar no while
            iteracoes(iteracao, X_atual, funcao(X_atual), "Sim", "Não", resultado_final)
        else: # X_1 não é raiz
            if abs(X_atual - X_anterior) < precisao_2: # Verifica a segunda condição de parada
                resultado_final = X_atual

                Continuar = False # Parar não entrar no while
                iteracoes(iteracao, X_atual, funcao(X_atual), "Não", "Sim", resultado_final)
            else:
                iteracoes(iteracao, X_atual, funcao(X_atual), "Não", "Não", "-")
                iteracao = iteracao + 1


    # Para X_2 ate X_n
    while Continuar:

        # o valor do X para a proxima iteracao X_(i+1)
        X_proximo = ((X_anterior * funcao(X_atual)) - ( X_atual * funcao(X_anterior))) / (funcao(X_atual) - funcao(X_anterior))
        X_anterior = X_atual
        X_atual = round(X_proximo, casas_decimais) # O numero de casas apos a virgula



        if abs(funcao(X_atual)) < precisao_1: # primeira condicao de parada para X_(i+1)
            resultado_final = X_atual

            Continuar = False # Para sair do while
            iteracoes(iteracao, X_atual, funcao(X_atual), "Sim", "Não", resultado_final)
        else:
            if abs(X_atual - X_anterior) < precisao_2: # segunda condicao de parada para X_(i+1)
                resultado_final = X_atual

                Continuar = False # Para sair do while
                iteracoes(iteracao, X_atual, funcao(X_atual), "Não", "Sim", resultado_final)
            else:
                iteracoes(iteracao, X_atual, funcao(X_atual), "Não", "Não", "-")
                iteracao = iteracao + 1
                if iteracao == iteracao_maxima: #Limita o numero de execuçoes caso não ache uma raiz
                    Continuar = False
                    resultado_final = X_atual
                    iteracoes(iteracao, X_atual, funcao(X_atual), "Não", "Não", resultado_final)

    print("")
    print("")




def cabecalho(equacao, A, B, X_0, X_1, precisao_1, precisao_2):
    print("")
    print("  Metódo da Secante")
    print("")
    print("")

    print(f'{"  A: ":<3} {A:<10} {"    ":^4} {"B: ":<3} {B:<10} {"    ":^4} {"X_0: ":<3} {X_0:<10} {"    ":^4} {"X_1: ":<3} {X_1:<10}  ')
    print("")
    print(f'{"  Epsilon_1: ":<3} {precisao_1:<10} {"    ":^4} {"Epsilon_2: ":<3} {precisao_2:<10} ')
    print("")
    print("  Função: " + equacao)

    print("")
    print("")
    espaco = "   " # 3 espacos
    string_1 = "  " + "i" + espaco + " X_i      "
    string_2 = espaco + " F(X_i)      " + espaco + "|f(X_i)|< E_1" + espaco + "|X_i - X_(i-1)|< E_2"
    string_3 = espaco + "Resultado"
    print(string_1 + string_2 + string_3)
    print("-------------------------------------------------------------------------------------")



def iteracoes(iteracao, X_atual, f_x, parada_1, parada_2, resultado_final):
    print( f'{iteracao:^5} {" ":^1}{X_atual:<10.7f} {" ":^2}{f_x:<10.7f} {" ":^1}{parada_1:^17} {" ":^1}{parada_2:^17} {" ":^6}{resultado_final:^10} ' )
    print("-------------------------------------------------------------------------------------")

    # f'{iteracao:^5} ' = f'{}' estrutura pra formatacao dentro fica a " iteracao: " é a variavel seguido de : e o numero de espaços que a variavel vai ocupar
    # ^ significa que o que esta na variavel vai ficar centralizado nos 5 espaços
    # < significa que o que esta na variavel vai ficar alinhado a esquerda nos 5 espaços
    # > significa que o que esta na variavel vai ficar alinhado a direita  nos 5 espaços
