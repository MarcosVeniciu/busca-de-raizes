import math  # funcoes matematicas https://docs.python.org/3/library/math.html
casas_decimais = 4


def binario_decimal():
    binario = "1010"

    decimal = 0

    cabecalho_binario(binario)
    indice = len(binario) -1
    gambiarra = 1

    print("  b_{0} = a_{0} >>> {1}".format(indice, binario[0]))
    v_anterior = binario[0]
    print("  ------------------")
    for numero in binario:
        decimal = int(numero) + (2*decimal)
        if gambiarra >= 2:
            passos_binario(indice, numero, decimal, v_anterior)
            v_anterior = decimal
        gambiarra = gambiarra + 1
        indice = indice -1

    print("")
    print("")



def decimal_fracionario_binario():
    r = 0.4021


    cabecario_fracionario(r)
    resultado = "0."
    iteracao = 1
    iteracao_maxima = 16

    # Para r_0
    if (2*r) < 1 :
        d = 0
        resultado = "{0}{1}".format(resultado, d)
        condicao = "{0} < 1".format((2*r))
        passo_fracionario(iteracao, r, condicao, d, resultado)
        iteracao = iteracao +1

    else:
        d = 1
        resultado = "{0}{1}".format(resultado, d)
        condicao = "{0} < 1".format((2*r))
        passo_fracionario(iteracao, r, condicao, d, resultado)
        iteracao = iteracao +1

    r = (2*r - d)
    r = round(r, casas_decimais) # r(k+1) onde d é dk
    if r == 0 :
        continuar = False
    else:
        continuar = True


    # Para r-1 ate r_n
    while continuar:
        if (2*r) < 1 :
            d = 0
            resultado = "{0}{1}".format(resultado, d)
            condicao = "{0} - {1} < 1".format((2*r), d)
            passo_fracionario(iteracao, r, condicao, d, resultado)
            iteracao = iteracao +1

        else:
            d = 1
            resultado = "{0}{1}".format(resultado, d)
            condicao = "{0:} - {1} < 1".format((2*r), d)
            passo_fracionario(iteracao, r, condicao, d, resultado)
            iteracao = iteracao +1

        r = 2*r - d
        r = round(r, casas_decimais) # r(k+1) onde d é dk
        if r == 0 :
            continuar = False
        if iteracao == iteracao_maxima:
            continuar = False
    print("")
    print("")



def cabecario_fracionario(valor):
    print("")
    print(" Converter Decimal Fracionario em Binario")
    print("")
    print(" Valor decimal na base 10: {0}".format(valor))
    print("")
    print("")
    print(" i    r           2*r - d_(i-1) < 1       d        Resultado (binario)")
    print("----------------------------------------------------------------------")

def passo_fracionario(indice, R, condicao, D, resultado):
    print(" {0:<4} {1:<11.7} {2:<16}        {3:<2}        {4:<19}".format(indice, R, condicao, D, resultado))
    print("----------------------------------------------------------------------")

def cabecalho_binario(binario):
    print("")
    print(" Converter Binario para Decimal")
    print("")
    print("")
    print(" Numero em Binario: {0}".format(binario))
    print("")
    print("")

def passos_binario(indice, v_binario, v_decimal, v_anterior):
    print("  b_{0} = a_{0} + 2*b_{2} >>> {1} + (2 * {4}) = {3}".format(indice, v_binario, (indice+1), v_decimal, v_anterior))
    print("  ----------------------------------------")
