import math  # funcoes matematicas https://docs.python.org/3/library/math.html
casas_decimais = 7

def funcao(x):
    reultado =  x + 1

    return round(reultado, casas_decimais) # O numero de casas apos a virgula

def Phi_x(x):
    derivada = x + 1

    return round(derivada, casas_decimais) # O numero de casas apos a virgula


def PontoFixo():
    A = 0
    B = 1
    x = 0.5

    precisao_1 = 0.0005
    precisao_2 = 0.0005


    equacao = "x^3 + 9*x + 3"
    funcao_iteracao = " (x^3 / 9) + (1/3)"

    iteracao = 0
    iteracao_maxima = 20


    cabecalho(equacao, funcao_iteracao, A, B, x, precisao_1, precisao_2)

    # Para i_0
    if abs(funcao(x)) < precisao_1: # Verifica a primeira condição de parada
        Continuar = False
        iteracoes(iteracao, x, funcao(x), Phi_x(x), "Sim", "-", x)
    else:
        Continuar = True
        iteracoes(iteracao, x, funcao(x), Phi_x(x), "Não", "-", "-")
        iteracao = iteracao +1

    # Para i_1 ate i_n
    while Continuar:
        x_anterior = x
        x = Phi_x(x_anterior)

        if abs(funcao(x)) < precisao_1: # Verifica a primeira condiçao de parada
            Continuar = False
            iteracoes(iteracao, x, funcao(x), Phi_x(x), "Sim", "Não", x)
        else:
            if abs(x - x_anterior) < precisao_2: # Verifica a segunda condiçao de parada
                Continuar = False
                iteracoes(iteracao, x, funcao(x), Phi_x(x), "Não", "Sim", x)
            else:
                iteracoes(iteracao, x, funcao(x), Phi_x(x), "Não", "Não", "-")
                iteracao = iteracao +1
                if iteracao == iteracao_maxima: # limita a quantidade de buscas a serem feitas
                    Continuar = False
                    iteracoes(iteracao, x, funcao(x), Phi_x(x), "Não", "Não", x)
    print("")
    print("")



def cabecalho(equacao, Phi, A, B, X, precisao_1, precisao_2):
    print("")
    print("  Metódo do Ponto Fixo")
    print("")
    print("")
    print(" A: {0:<10}  B: {1:<10}  X: {2:<10}".format(A, B, X))
    print("")
    print(" Epsilon_1: {0:<10}  Epislon_2: {1:<10}".format(precisao_1, precisao_2))
    print("")
    print(" Função: {0}".format(equacao))
    print("")
    print(" Função de iterão (phi): {0}".format(Phi))
    print("")
    print("")
    print("  i   X            F(x)        Phi(x)       |f(X)|< E_1    |X_i - X_(i-1)|< E_2     Resultado" )
    print("---------------------------------------------------------------------------------------------")



def iteracoes(iteracao, x, f_x, phi_x, parada_1, parada_2, resultado):

    print("  {0:<2}  {1:<10.7f}   {2:<10.7f}  {3:<10.7f}  {4:^11}    {5:^20}     {6:^9}".format(iteracao, x, f_x, phi_x, parada_1, parada_2, resultado))
    print("---------------------------------------------------------------------------------------------")
    #https://programadorviking.com.br/metodo-format-em-python/
